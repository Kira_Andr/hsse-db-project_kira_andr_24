-- Удобно иметь информацию о команде и их игроках в одной табличке.
create view players_in_teams as
select first_name,
       second_name,
       age,
       player_id,
       volleyball_matches.teams.team_id,
       name
from volleyball_matches.players
         join volleyball_matches.teams
              on teams.team_id = players.team_id;

-- Вьюшки для игроков мужского и женского пола соответственно
create or replace view male_players as
select first_name,
       second_name,
       age,
       player_id,
       team_id
from volleyball_matches.players
where sex = 'male';

create or replace view female_players as
select first_name,
       second_name,
       age,
       player_id,
       team_id
from volleyball_matches.players
where sex = 'female';

-- Вью для подсчёта количества выигранных матчей каждой команды
create or replace view count_matches_won as

with won_first as (select team_id,
                          name,
                          count(*) as cnt
                   from volleyball_matches.teams
                            join volleyball_matches.matches m on teams.team_id = m.team_a
                   where ((split_part(set_1, '-', 1)::int > split_part(set_1, '-', 2)::int)::int +
                          (split_part(set_2, '-', 1)::int > split_part(set_2, '-', 2)::int)::int +
                          (split_part(set_3, '-', 1)::int > split_part(set_3, '-', 2)::int)::int) >= 2
                   group by team_id, name),

won_second as (select team_id,
                      name,
                      count(*) as cnt
                      from volleyball_matches.teams
                      join volleyball_matches.matches m on teams.team_id = m.team_b
                      where ((split_part(set_1, '-', 1)::int < split_part(set_1, '-', 2)::int)::int +
                            (split_part(set_2, '-', 1)::int < split_part(set_2, '-', 2)::int)::int +
                            (split_part(set_3, '-', 1)::int < split_part(set_3, '-', 2)::int)::int) >= 2
                      group by team_id, name)
select team_id,
       name,
       sum(cnt)::bigint as matches_won
from (select team_id,
             name,
             cnt
      from won_first
      union all
      select team_id,
             name,
             cnt
      from won_second) as concatenated
group by team_id, name
order by matches_won desc;

-- Вью для подсчёта количества сыгранных матчей каждой команды
create or replace view count_played_matches as

with won_first as (select team_id,
                          name,
                          count(*) as cnt
                   from volleyball_matches.teams
                            join volleyball_matches.matches m on teams.team_id = m.team_a
                   group by team_id, name),

won_second as (select team_id,
                      name,
                      count(*) as cnt
                      from volleyball_matches.teams
                      join volleyball_matches.matches m on teams.team_id = m.team_b
                      group by team_id, name)
select team_id,
       name,
       sum(cnt)::bigint as matches_played
from (select team_id,
             name,
             cnt
      from won_first
      union all
      select team_id,
             name,
             cnt
      from won_second) as concatenated
group by team_id, name
order by matches_played desc;