-- 1) Триггер на добавление матча: обновляет на стадионе дату последнего матча
create or replace function update_date_last_match_fnc() returns trigger as
$$
begin
    update volleyball_matches.stadiums as s
    set date_last_match = new.date
    where s.stadium_id = new.stadium_id;
    return new;
end;
$$
    language plpgsql;

create or replace trigger update_date_last_match
    after insert
    on volleyball_matches.matches
    for each row
execute procedure update_date_last_match_fnc();

-- 2) Функция get_winner - принимает строку (резельтат сета) и возвращает 1, если команда А выиграла команду Б, иначе - 0
create or replace function get_winner(text)
    returns int
as
$$
select (split_part($1, '-', 1)::int > split_part($1, '-', 2)::int)::int
$$
    language sql
    immutable
    returns null on null input;

-- 3) Триггер на изменение возраста тренера: увеличивает на то же значение опыт работы
create or replace function update_trainer_age_fnc() returns trigger as
$$
begin
    if new.age <> old.age then
        update volleyball_matches.trainers as s
        set work_experience = old.work_experience + (new.age - old.age)
        where trainer_id = new.trainer_id;
    end if;

    return new;
end;
$$
    language plpgsql;

create or replace trigger update_trainer_age
    after update
    on volleyball_matches.trainers
    for each row
execute procedure update_trainer_age_fnc();

-- 4) Функция get_count_players - принимает team_id и возвращает количество игроков в этой команде.
create or replace function get_count_players(bigint) returns bigint as
    $$
    select count(*) as count
    from volleyball_matches.players p
        where p.team_id = $1;
    $$ language sql;
