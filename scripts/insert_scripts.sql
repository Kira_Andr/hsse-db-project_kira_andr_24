-- inserting into Teams. Total 10.
insert into volleyball_matches.teams (name, country, city, league)
values ('Dream team', 'USA', 'New-York', 'professionals'),
       ('Russian Bears', 'Russia', 'Moscow', 'professionals'),
       ('Team Spirit', 'Russia', 'St. Petersburg', 'beginners'),
       ('MIPT', 'Russia', 'Dolgoprudny', 'beginners'),
       ('Sunrise', 'Italy', 'Rome', 'middle'),
       ('Black car', 'Germany', 'Berlin', 'middle'),
       ('Zenit', 'Russia', 'St. Petersburg', 'professionals'),
       ('Torpedo', 'Russia', 'Vladimir', 'middle'),
       ('Old school', 'Germany', 'Berlin', 'beginners'),
       ('Andromeda', 'Russia', 'Izhevsk', 'professionals');

-- inserting into Stadions. Total 10.
insert into volleyball_matches.stadiums (country, city, count_seats, date_building, date_last_match)
values ('Russia', 'Moscow', 5000, '2006-03-18', '2024-04-12'),
       ('Russia', 'St. Petersburg', 3000, '2011-05-16', '2022-12-20'),
       ('USA', 'New-York', 10000, '2015-06-05', '2023-08-19'),
       ('Russia', 'Vladimir', 4000, '2004-02-20', '2024-03-17'),
       ('USA', 'Washington', 7000, '2010-10-10', '2023-04-08'),
       ('Russia', 'Moscow', 3500, '2010-07-17', '2024-03-05'),
       ('Germany', 'Berlin', 4000, '2016-11-26', '2023-12-21'),
       ('Russia', 'Moscow', 11000, '2024-02-16', '9999-12-31'),
       ('Russia', 'St. Petersburg', 1000, '2015-08-15', '2022-04-19'),
       ('Italy', 'Rome', 5000, '2017-04-18', '2024-04-08');

-- inserting into Trainers. Total 10.
insert into volleyball_matches.trainers (first_name, second_name, age, team_id, work_experience)
values ('Nikita', 'Tseleshkov', 25,
        (select team_id from volleyball_matches.teams where name = 'Russian Bears'), 5),
       ('Kirill', 'Andreev', 41,
        (select team_id from volleyball_matches.teams where name = 'MIPT'), 24),
       ('Aleksandr', 'Tylenev', 36,
        (select team_id from volleyball_matches.teams where name = 'Zenit'), 12),
       ('Oleg', 'Ivanov', 64,
        (select team_id from volleyball_matches.teams where name = 'Torpedo'), 42),
       ('Nicolas', 'Keidg', 48,
        (select team_id from volleyball_matches.teams where name = 'Dream team'), 14),
       ('Vlad', 'Shwarts', 55,
        (select team_id from volleyball_matches.teams where name = 'Team Spirit'), 28),
       ('Francesco', 'Linad', 74,
        (select team_id from volleyball_matches.teams where name = 'Sunrise'), 40),
       ('Gans', 'Smid', 56,
        (select team_id from volleyball_matches.teams where name = 'Black car'), 22),
       ('Gans', 'Shtirlets', 61,
        (select team_id from volleyball_matches.teams where name = 'Old school'), 23),
       ('Daniil', 'Kramar', 32,
        (select team_id from volleyball_matches.teams where name = 'Andromeda'), 5);

-- inserting into Players. Total 13.
insert into volleyball_matches.players (first_name, second_name, age, team_id, sex)
values ('Kirill', 'Zaitsev', 19,
        (select team_id from volleyball_matches.teams where name = 'Russian Bears'), 'male'),
    ('Mihail', 'Krug', 22,
        (select team_id from volleyball_matches.teams where name = 'Russian Bears'), 'male'),
    ('Aleksandr', 'Black', 18,
        (select team_id from volleyball_matches.teams where name = 'Russian Bears'), 'male'),
       ('Denis', 'Kolesnikov', 24,
        (select team_id from volleyball_matches.teams where name = 'MIPT'), 'male'),
       ('Aleksandr', 'Jonrea', 28,
        (select team_id from volleyball_matches.teams where name = 'Zenit'), 'male'),
       ('Ivan', 'Ivanov', 27,
        (select team_id from volleyball_matches.teams where name = 'Torpedo'), 'male'),
       ('Jony', 'Depp', 26,
        (select team_id from volleyball_matches.teams where name = 'Dream team'), 'male'),
       ('Maxim', 'Sredny', 31,
        (select team_id from volleyball_matches.teams where name = 'Team Spirit'), 'male'),
       ('Violetta', 'Ruotti', 22,
        (select team_id from volleyball_matches.teams where name = 'Sunrise'), 'female'),
       ('Malley', 'Gotry', 21,
        (select team_id from volleyball_matches.teams where name = 'Sunrise'), 'female'),
       ('Mark', 'Serids', 24,
        (select team_id from volleyball_matches.teams where name = 'Black car'), 'male'),
       ('Swids', 'Gerdats', 33,
        (select team_id from volleyball_matches.teams where name = 'Old school'), 'male'),
       ('Timur', 'Vasiliev', 28,
        (select team_id from volleyball_matches.teams where name = 'Andromeda'), 'male');

-- inserting into Matches. Total 11.
insert into volleyball_matches.matches (stadium_id, team_a, team_b, date, set_1, set_2, set_3)
values (1, 1, 2, '2024-04-12', '25 - 23', '20 - 25', '18 - 25'),
       (2, 7, 4, '2022-12-20', '25 - 15', '25 - 19', '25 - 22'),
       (5, 8, 6, '2023-04-08', '28 - 26', '30 - 28', '16 - 25'),
       (5, 9, 1, '2024-02-12', '18 - 25', '20 - 25', '25 - 15'),
       (6, 3, 5, '2024-03-05', '25 - 21', '25 - 22', '27 - 25'),
       (6, 5, 1, '2022-04-19', '22 - 25', '27 - 25', '23 - 25'),
       (9, 4, 9, '2022-04-19', '25 - 23', '16 - 25', '13 - 25'),
       (9, 1, 2, '2021-08-12', '19 - 25', '20 - 25', '25 - 18'),
       (1, 10, 9, '2023-12-05', '25 - 17', '23 - 25', '25 - 18'),
       (3, 7, 3, '2023-08-19', '25 - 27', '30 - 28', '21 - 25'),
       (4, 6, 10, '2024-03-17', '19 - 25', '16 - 25', '18 - 25');