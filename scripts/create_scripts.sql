-- Creating tables

-- Creating schema of databases of volleyboll matches
create schema volleyball_matches;

-- Information about teams
create table if not exists volleyball_matches.teams (
    team_id serial PRIMARY KEY,
    name    text not null check (name != ''),
    country text not null,
    city    text not null,
    league  text not null
);

-- Information about players
create table if not exists volleyball_matches.players (
    player_id   serial primary key,
    first_name  text not null check (first_name != ''),
    second_name text not null check (second_name != ''),
    age         integer  not null check (age > 0),
    team_id     serial references volleyball_matches.teams (team_id),
    sex         text not null CHECK (sex in ('male', 'female'))
);

-- Information about stadiums
create table if not exists volleyball_matches.stadiums (
    stadium_id      serial primary key,
    country         text not null,
    city            text not null,
    count_seats     integer  not null,
    date_building   date not null,
    date_last_match date
);

-- Information about matches
create table if not exists volleyball_matches.matches (
    match_id   serial primary key,
    stadium_id serial references volleyball_matches.stadiums (stadium_id),
    team_A     serial references volleyball_matches.teams (team_id),
    team_B     serial references volleyball_matches.teams (team_id),
    date       date not null,
    set_1      text check (set_1 like '% - %'),
    set_2      text check (set_2 like '% - %'),
    set_3      text check (set_3 like '% - %')
);

-- Information about trainers
create table if not exists volleyball_matches.trainers (
    trainer_id      serial PRIMARY KEY,
    first_name      text not null,
    second_name     text not null,
    age             integer  not null check (age > 0),
    team_id         serial references volleyball_matches.teams (team_id),
    work_experience integer  not null
);