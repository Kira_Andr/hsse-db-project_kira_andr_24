-- Increase legue of team
update volleyball_matches.teams
set league = 'middle'
where name = 'MIPT';

-- Trainer celebrated his birthday
update volleyball_matches.trainers
set age = age + 1, work_experience = work_experience + 1
where trainer_id = 5;

-- All players have grown up by a year
update volleyball_matches.players
set age = age + 1;

-- Was a new match at stadium 8
update volleyball_matches.stadiums
set date_last_match = '2024-04-13'
where stadium_id = 8;

-- Team changed their city
update volleyball_matches.teams
set country = 'Vladimir'
where name = 'Russian Bears';