-- Вывести id и название всех команд из России.
select team_id, name
from volleyball_matches.teams
where country = 'Russia'
order by team_id;

-- Вывести всех игроков, которые играют за Московские команды.
select first_name, second_name, teams.name
from volleyball_matches.players as players
         join volleyball_matches.teams as teams
              on players.team_id = teams.team_id
where teams.city = 'Moscow';

-- Вывести количество команд в каждой стране.
select count(*) as count, country
from volleyball_matches.teams
group by country
order by count desc;

-- Вывести количнество мужчин и женщин играющих не за Росиийские команды.
select count(*), sex
from volleyball_matches.players p
         join volleyball_matches.teams t on t.team_id = p.team_id
where lower(t.country) != 'russia'
group by sex;

-- Вывести страны в алфовитном порядке страны, в которых не менее двух стадионов и их количество
select count(*) as count, country
from volleyball_matches.stadiums
group by country
having count(*) >= 2
order by lower(country);

-- Вывести топ 5 тренеров по опыту работу. Проранжировать их
select dense_rank() over (order by work_experience desc) as rank,
       first_name,
       second_name,
       work_experience
from volleyball_matches.trainers
order by rank
limit 5;

-- Вывести среднее количество мест на стадионах для каждой страны и города.
select country,
       city,
       date_building,
       round(avg(count_seats) over (partition by country)) as avg_count_seats_in_country,
       round(avg(count_seats) over (partition by city))    as avg_count_seats_in_city
from volleyball_matches.stadiums
order by country, city desc;

-- Вывести разницу в процентах в возрасте между тренерами
with trainers_new as (select first_name,
                             second_name,
                             work_experience,
                             lag(work_experience, 1) over w as prev
                      from volleyball_matches.trainers
                      window w as (order by work_experience))
select first_name,
       second_name,
       work_experience,
       concat(case
                  when round((work_experience - prev) * 100.0 / prev) is null then '0'
                  else round((work_experience - prev) * 100.0 / prev)::text end, '%') as diff
from trainers_new
order by work_experience;

-- Проранжировать стадионы внутри одной страны по количеству мест.
select country,
       city,
       date_building,
       count_seats,
       rank() over w as rank
from volleyball_matches.stadiums
window w as (
        partition by country
        order by count_seats )
order by country, rank;

-- Разбить тренеров на 3 группы по возрасту
select ntile(3) over w as tile,
       first_name,
       second_name,
       age
from volleyball_matches.trainers
window w as (
        order by age desc
        )
order by age desc;

-- Вывести топ 5 самых молодых игроков и команду, в которой они играют. Проранжировать их.
select first_name,
       second_name,
       name,
       age,
       rank
from (select dense_rank() over (order by age) as rank,
             team_id,
             first_name,
             second_name,
             age
      from volleyball_matches.players
      order by rank
      limit 5) as ranged_players
         join volleyball_matches.teams as t
              on t.team_id = ranged_players.team_id
order by rank;